FROM alpine/git AS downloader
RUN mkdir -p /var/www \
    # --branch value can be a tag name when building image for a specific release/version(e.g. tags/26.2) or a branch name to build an image using the latest code of that branch.
    && git clone --depth 1 --branch=26.x --single-branch https://gitlab.com/tikiwiki/tiki.git /var/www/html

FROM php:8.1.27-apache AS php

RUN apt-get update \
    && apt-get install -y \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libldap2-dev \
        libldb-dev \
        libmemcached-dev \
        libonig-dev \
        libpng++-dev \
        libzip-dev \
        unzip \
        zlib1g-dev \
        libssl-dev \
    && ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so \
    && ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install bcmath calendar gd intl ldap mysqli mbstring pdo_mysql zip \
    && printf "yes\n" | pecl install xdebug-3.2.2 \
    && printf "no\n"  | pecl install apcu-beta \
    && printf "no\n"  | pecl install memcached \
    && echo 'extension=apcu.so' > /usr/local/etc/php/conf.d/pecl-apcu.ini \
    && echo 'extension=memcached.so' > /usr/local/etc/php/conf.d/pecl-memcached.ini \
    && echo "extension=ldap.so" > /usr/local/etc/php/conf.d/docker-php-ext-ldap.ini \
    && apt-get purge -y \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libldap2-dev \
        libldb-dev \
        libmemcached-dev \
        libonig-dev \
        libpng++-dev \
        libzip-dev \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    && { \
        echo "file_uploads = On"; \
        echo "upload_max_filesize = 2048M"; \
        echo "post_max_size = 2048M"; \
        echo "max_file_uploads = 20"; \
    } > /usr/local/etc/php/conf.d/docker-uploads.ini \
    && mkdir -p /var/www/.composer /var/www/.config \
    && chown www-data:www-data /var/www/.composer /var/www/.config \
    && curl -s -L -o /usr/local/bin/composer https://getcomposer.org/download/latest-1.x/composer.phar \
    && chmod 755 /usr/local/bin/composer \
    && { \
        COMPOSER_HOME=/usr/local/share/composer \
        COMPOSER_BIN_DIR=/usr/local/bin \
        COMPOSER_CACHE_DIR="/tmp/composer" \
        composer global require psy/psysh; \
    } \
    && rm -rf /tmp/*

COPY root/ /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["psysh"]

FROM php
LABEL maintainer "TikiWiki <tikiwiki-devel@lists.sourceforge.net>"

COPY --from=downloader /var/www/html /var/www/html
WORKDIR "/var/www/html"

RUN composer self-update --2 \
    && php \
        -d zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so) \
        /usr/local/bin/composer install --working-dir /var/www/html/vendor_bundled --prefer-dist \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \ 
    && rm -rf /root/.composer \
    && { \
        echo "<?php"; \
        echo "    \$db_tiki        = getenv('TIKI_DB_DRIVER') ?: 'mysqli';"; \
        echo "    \$dbversion_tiki = getenv('TIKI_DB_VERSION') ?: '26';"; \
        echo "    \$host_tiki      = getenv('TIKI_DB_HOST') ?: 'db';"; \
        echo "    \$user_tiki      = getenv('TIKI_DB_USER');"; \
        echo "    \$pass_tiki      = getenv('TIKI_DB_PASS');"; \
        echo "    \$dbs_tiki       = getenv('TIKI_DB_NAME') ?: 'tikiwiki';"; \
        echo "    \$client_charset = 'utf8mb4';"; \
        echo "    foreach (glob('/var/www/conf.d/*.php') as \$conf) { include(\$conf); }"; \
    } > /var/www/html/db/local.php \
    && {\
        echo "session.save_path=/var/www/sessions"; \
    }  > /usr/local/etc/php/conf.d/tiki_session.ini \
    && /bin/bash doc/devtools/htaccess.sh \
    && mkdir -p /var/www/sessions \
    && mkdir -p /var/www/conf.d \
    && chown -R www-data /var/www/sessions \
    && chown -R www-data /var/www/html/db/ \
    && chown -R www-data /var/www/html/img/trackers/ \
    && chown -R www-data /var/www/html/img/wiki/ \
    && chown -R www-data /var/www/html/img/wiki_up/ \
    && chown -R www-data /var/www/html/temp/ \
    && chown -R www-data /var/www/html/templates/

VOLUME ["/var/www/conf.d/","/var/www/html/files/","/var/www/html/img/trackers/","/var/www/html/img/wiki_up/","/var/www/html/img/wiki/","/var/www/html/modules/cache/","/var/www/html/storage/","/var/www/html/temp/","/var/www/sessions/"]
EXPOSE 80
CMD ["apache2-foreground"]
